#!/usr/bin/env python3;
import sys
import subprocess


def install(package):
    # This is an evil little function
    # that installs packages via pip.
    # This means the script can install
    # it's own dependencies.
    #try:
    #    __import__(package)
    #except:
	subprocess.call([sys.executable, "-m", "pip", "install", package])
		
		
install("papermill")
install("ipykernel")
install("psutil");
install("distro")
install("click==7.0")
install("xgboost")
install("matplotlib")
install("seaborn")
install("scikit-learn")
install("pandasql")
install("openpyxl")
install("gcsfs")
install("keras")


import papermill as pm
pm.execute_notebook('/home/airflow/gcs/dags/models/Xgboost_Predict.ipynb', '/home/airflow/gcs/dags/models/xgb-predict-papermill-output.ipynb', parameters={"model":"/tmp/xgb_model.pkl", "input_data":"gs://asia-east2-idil-1b26caca-bucket/dags/models/function_input_all.csv", "xgb_file":"/tmp/xgb_results.csv"});
        
import os
os.system('gsutil cp /tmp/xgb_model.pkl gs://asia-east2-idil-1b26caca-bucket/xgb_model.pkl')
os.system('gsutil cp /tmp/xgb_results.csv gs://asia-east2-idil-1b26caca-bucket/xgb_results.csv')
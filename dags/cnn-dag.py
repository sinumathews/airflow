import airflow
from datetime import timedelta
# The DAG object; we'll need this to instantiate a DAG
from airflow import DAG
# Operators; we need this to operate!
from airflow.operators.bash_operator import BashOperator
from airflow.utils.dates import days_ago
from airflow.models import Variable
import os

# These args will get passed on to each operator
# You can override them on a per-task basis during operator initialization
default_args = {
    'owner': 'HotelPrediction',
    'depends_on_past': False,
    'start_date': days_ago(1),
    'email': ['example@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
	'setup_file': '/home/airflow/gcs/dags/setup.py',
    # 'queue': 'bash_queue',
    # 'pool': 'backfill',
    # 'priority_weight': 10,
    # 'end_date': datetime(2016, 1, 1),
    # 'wait_for_downstream': False,
    # 'dag': dag,
    # 'sla': timedelta(hours=2),
    # 'execution_timeout': timedelta(seconds=300),
    # 'on_failure_callback': some_function,
    # 'on_success_callback': some_other_function,
    # 'on_retry_callback': another_function,
    # 'sla_miss_callback': yet_another_function,
    # 'trigger_rule': 'all_success'
}
	
cnn_input_script = Variable.get("cnn_input_script")
cnn_output_script = Variable.get("cnn_output_script")
cnn_model = Variable.get("cnn_model")
cnn_pkl_file = Variable.get("cnn_pkl_file")
cnn_results = Variable.get("cnn_results")
cnn_input_data = Variable.get("cnn_input_data")


#instantiates a directed acyclic graph
dag = DAG(
    'cnn-dag',
    default_args=default_args,
    description='A Machine Learning CNN pipeline',
    schedule_interval=timedelta(minutes=5),
)

papermill_command = "sudo python3 /home/airflow/gcs/dags/cnn-scripts.py"
	
predict_model = BashOperator(
    task_id='predict_model',
    bash_command=papermill_command,
	retries=3,
    dag=dag,
)

upload_command = """

"""
	
upload_files = BashOperator(
    task_id='upload_files',
    bash_command=upload_command,
	retries=3,
    dag=dag,
)

#sets the ordering of the DAG. The >> directs the 2nd task to run after the 1st task. This means that
#download images runs first, then train, then serve.
predict_model >> upload_files

if __name__ == "__main__":
    dag.cli()
#!/usr/bin/env python3;
import sys
import subprocess


def install(package):
    # This is an evil little function
    # that installs packages via pip.
    # This means the script can install
    # it's own dependencies.
    #try:
    #    __import__(package)
    #except:
	subprocess.call([sys.executable, "-m", "pip", "install", package])
		
		
install("papermill")
install("ipykernel")
install("psutil");
install("distro")
install("click==7.0")
install("xgboost")
install("matplotlib")
install("seaborn")
install("scikit-learn")
install("pandasql")
install("openpyxl")
install("gcsfs")
install("keras")

	
import os
os.system('gsutil cp gs://asia-east2-idil-1b26caca-bucket/dags/cnn-models/cnn_model.h5 /tmp/cnn_model.h5')
os.system('gsutil cp gs://asia-east2-idil-1b26caca-bucket/dags/cnn-models/cnn.pkl /tmp/cnn.pkl')
os.system('gsutil cp gs://asia-east2-idil-1b26caca-bucket/dags/cnn-models/hbsi_actual_results.csv /tmp/hbsi_actual_results.csv')

import papermill as pm
pm.execute_notebook('/home/airflow/gcs/dags/cnn-models/CNN_model-predict-idil.ipynb', '/home/airflow/gcs/dags/cnn-models/cnn-papermill-output.ipynb', parameters={"cnn_model":"/tmp/cnn_model.h5", "cnn_pkl_file":"/tmp/cnn.pkl", "cnn_results":"/tmp/cnn_results.csv", "cnn_input_data":"/tmp/hbsi_actual_results.csv"});

os.system('gsutil cp /tmp/cnn_results.csv gs://asia-east2-idil-1b26caca-bucket/cnn_results.csv')
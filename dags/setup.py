from setuptools import find_packages, setup

setup(
    name="Common",
    version="1.0.0",
    install_requires=['papermill==2.1.3'],
    description=("Custom python utils"),
    packages=find_packages(),
    include_package_data=False)